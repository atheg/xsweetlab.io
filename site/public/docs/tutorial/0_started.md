# 0. Create a new PubSweet app

To start, you'll need to create a new PubSweet app to work in. First, make sure [PubSweet is properly installed](/docs/misc/prerequisites.html). Then, follow these steps:

1. Run `pubsweet new [your_app_name]` to generate a new app
2. `cd` into the newly created `[your_app_name]` directory
3. Run `pubsweet start` to start a PubSweet server
4. In your browser, navigate to `localhost:3000`. If everything is working correctly, the PubSweet app will load
5. Verify that you can login by going to `localhost:3000/login` and entering your credentials

For this tutorial, you'll want to run PubSweet in dev mode:
1. Run `pubsweet setupdb` in the app's root directory to create the dev database
2. `pubsweet start` starts a server running in dev mode

Dev mode allows changes to the code to be hot loaded into the app without having to restart the server. It also enables logs state and action changes to the console for easy debugging. You can see these changes and console logs by opening your browser's developer tools and finding the console. You'll also want to use your favorite text editor for updating code.

If you've got a running PubSweet, then you're ready to [make a component](./1_first.html).
