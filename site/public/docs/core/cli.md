# CLI

[![npm](https://img.shields.io/npm/v/pubsweet.svg)](https://npmjs.com/package/pubsweet)
[![MIT license](https://img.shields.io/badge/license-MIT-e51879.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-cli.svg/raw/master/LICENSE)
[![code style standard](https://img.shields.io/badge/code%20style-standard-green.svg)](https://standardjs.com/)
[![coverage report](https://gitlab.coko.foundation/pubsweet/pubsweet-cli/badges/master/coverage.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-cli)
[![build status](https://gitlab.coko.foundation/pubsweet/pubsweet-cli/badges/master/build.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-cli)

The `pubsweet` module provides a command-line interface for generating, managing and running PubSweet apps.

## Getting PubSweet CLI

### Prerequisites

- node v8.9+ (LTS)
- yarn v1.3.2+ (required to run `pubsweet new`)
- npm v5+

We recommend using [`nvm`](https://github.com/creationix/nvm) to install and manage Node.js and nvm versions. See [detailed instructions on installing prerequisites](/docs/misc/prerequisites.html) on Mac, Linux, and PC. Note that PubSweet is not fully supported on PC; if you use Windows 10, we recommend using the Bash terminal for Windows.

### Installation

The PubSweet command-line tools can be installed from `npm` or `yarn`:

```bash
npm install --global pubsweet
```

or

```bash
yarn global add pubsweet
```

## Using PubSweet CLI

### Displaying the commands (`pubsweet` or `pubsweet help`)

Outputs:

```bash
Usage: pubsweet [options] [command]


Commands:

    new         create and set up a new pubsweet app
    setupdb     generate a database for a pubsweet app
    build       build static assets for a pubsweet app
    start       build static assets and start a pubsweet app
    add         add one or more components to a pubsweet app
    remove      remove one or more components from a pubsweet app
    adduser     add a user to the database for a pubsweet app
    
Options:

  -h, --help     output usage information
  -V, --version  output the version number
```

For any command, you can see detailed usage help by running `pubsweet help cmd`, e.g.:

```bash
pubsweet help new
pubsweet help start
# ...etc
```

### Generating an app (`pubsweet new`)

The `new` subcommand generates a template PubSweet app for you to work from. This includes everything needed to run your publishing platform: dependencies, database setup, boilerplate code and configuration, and a set of initial components.

To generate an app, just provide the name:

```bash
pubsweet new myappname
```

`pubsweet` will create a subdirectory with the name you supplied, and start generating the app inside. It'll ask you a series of questions to customise your app. If you prefer to provide the answers in the initial command, you can do that instead:

```bash
pubsweet new myappname \
  --username someuser \
  --email some@email.com \
  --password correct-horse-battery-staple \
```
See these additional instructions for [generating and running a new PubSweet app on Windows 10](/docs/misc/prerequisites.html#pc-note).

### Running your app (`pubsweet run`)

The `run` subcommand starts your app. It takes care of transpilation, module bundling and process management.

To start your app use the `start` command from the app directory:

```bash
cd myappname
pubsweet start
```

### Setting up the database (`pubsweet setupdb`)

The `setupdb` subcommand creates the database for your app. It is usually used when you've cloned the source code of an existing app, or when you want to start over with a fresh database.

To generate a database for an app that doesn't have one yet:

```bash
pubsweet setupdb
```

By default this will generate a **production** database only. To generate a **dev** database, run the command with `NODE_ENV=development`:

```bash
NODE_ENV=development pubsweet setupdb 
```

If your app already has a database, `pubsweet setupdb` will not overwrite it by default. You can force it to delete an existing database and overwrite it with a new one using `--clobber`:

```bash
$ pubsweet setupdb
info: Generating PubSweet app database at path api/db/production
error: Database appears to already exist
error: If you want to overwrite the database, use --clobber
$ pubsweet setupdb --clobber
info: Generating PubSweet app database at path api/db/production
info: Database appears to already exist
info: Overwriting existing database due to --clobber flag
info: setting up the database
info: building prompt
question:><Admin username><
[...etc]
```

As with `pubsweet new`, you can skip any or all of the interactive prompts by providing the user details with command-line flags:

```bash
pubsweet setupdb  \
  --username someuser \
  --email some@email.com \
  --password correct-horse-battery-staple \
```

### Adding a user to the database (`pubsweet adduser`)

You can add a user to an existing database:

```bash
pubsweet adduser
```

You can optionally make that user an admin:

```bash
pubsweet adduser --admin
```

As with `pubsweet new` and `pubsweet setupdb`, you can skip any or all of the interactive prompts by providing the user details with command-line flags:

```bash
pubsweet adduser \
  --username someuser \
  --email some@email.com \
  --password correct-horse-battery-staple \
  --admin true
```
