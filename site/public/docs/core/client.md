# Client

[![npm](https://img.shields.io/npm/v/pubsweet-client.svg)](https://npmjs.com/package/pubsweet-client)
[![MIT license](https://img.shields.io/badge/license-MIT-e51879.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-client/raw/master/LICENSE)
[![code style standard](https://img.shields.io/badge/code%20style-standard-green.svg)](https://standardjs.com/)
[![coverage report](https://gitlab.coko.foundation/pubsweet/pubsweet-client/badges/master/coverage.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-client)
[![build status](https://gitlab.coko.foundation/pubsweet/pubsweet-client/badges/master/build.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-client)

The `pubsweet-client` module creates the app that runs in the user's browser. It handles the [user interface](#ui), [routing](#routing), and optionally [state management](#state).

<h2 id="ui">
  <a name="ui"></a>
  User interface
</h2>

`pubsweet-client` is a [React](https://facebook.github.io/react/) app. In general any React components should *just work* in a PubSweet app.

The PubSweet client uses [JSX](https://facebook.github.io/react/docs/introducing-jsx.html). This means the client must be transpiled to plain JS. However, a PubSweet app in general can be extended [without using JSX](https://facebook.github.io/react/docs/react-without-jsx.html), so you don't need to learn JSX to use PubSweet.

<h2 id="routing">
  <a name="routing"></a>
  Routing
</h2>

`pubsweet-client` uses [`react-router`](https://github.com/reacttraining/react-router) for routing between views of an app.

By convention we store routes in [`app/routes.jsx`](https://gitlab.coko.foundation/pubsweet/pubsweet-starter/blob/master/app/routes.jsx).

Adding or modifying routes involves simply editing the `routes.jsx` file.

<h2 id="state">
  <a name="state"></a>
  State management
</h2>

[Redux](http://redux.js.org/) is used for app-wide state management. Redux uses `actions` to handle spawning state-changing effects, and `reducers` to handle updating the state based on the outcome of `actions`.

`pubsweet-client` includes some [`actions`](https://gitlab.coko.foundation/pubsweet/pubsweet-client/tree/master/src/actions) and [`reducers`](https://gitlab.coko.foundation/pubsweet/pubsweet-client/tree/master/src/reducers) for sending information to the `pubsweet-server` REST API and applying the results to the Redux state.

Any component can also manage its own state using internal variables, and choose not to interact with the Redux store at all.
