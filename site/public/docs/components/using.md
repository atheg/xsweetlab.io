# Using components

Using a component is as simple as installing it, configuring it, and then including the functionality it exports in your own app code.

## Installing

Components can be installed using the `pubsweet` cli:

```bash
pubsweet add login
```

This installs the `pubsweet-component-login` package, and adds the component to the app config so PubSweet knows to load it.

Note that `pubsweet add` adds the `pubsweet-component-` prefix automatically if it's not already present.

You can also manually install components using `npm` or `yarn`:

```bash
npm install --save pubsweet-component-login
```

or

```bash
yarn add pubsweet-component-login
```

If you do that, you'll need to add the added components to the [configuration](#configuring).

## Configuring

There are two aspects to configuring components:

1. the app needs to know which components to load
2. the component can be configured

### Specifying which components to load

PubSweet will load any components in the `pubsweet.components` array of the configuration object.

You can add components to this array manually, or if you install the component using `pubsweet add` they will be added for you.

In a `pubsweet` CLI generated app, the list of loaded components is in its own JSON file at `./config/components.json`.

*note*: you need to use the full npm package name in the config array, i.e. `pubsweet-component-login` not `login`

### Configuring a component

Any component can define its own configurable options, and can access general configuration from the client or server.

Each component's README should specify which configuration options can be used, and what they do.
