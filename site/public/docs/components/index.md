# PubSweet Components

A **PubSweet component** is a [Node.js module](https://nodejs.org/api/modules.html) that can extend or change functionality of the [server](/docs/core/server.html) and/or [client](/docs/core/client.html) of a PubSweet app.

Components build on the core modules to provide the actual functionality of a PubSweet app. By combining the components you need, you can build any publishing platform or workflow you can imagine.

Start [using them straight away](#next-steps), or see how components work with [an example](#example).

<h3 id="next-steps">Next steps</h3>

- Learn [how to use components](using.html) in your app.
- Take a look at the [component library](library.html) to see what existing components can do
- See our [component developer guide](developing.html) to learn how to create your components.

<h3><span id="example">An example: user login</span></h3>

The [`login`](https://gitlab.coko.foundation/pubsweet/pubsweet-components/tree/master/packages/Login) component provides basic user login functionality in the client. It does this by exporting:

- **redux actions** for authenticating and de-authenticating users with the server
- **redux reducers** for managing user data and authentication tokens
- a **React login form** that provides a login interface and uses the login redux actions and reducers
- a **default style** for the form

You can [install the `login` component](using.html) and instantly have login functionality in your app. You can also pick and choose which parts of the component you use. You example you could build your own React login form and still make use of the redux actions and reducers from the `login` component, or keep the existing form but provide a new style.
