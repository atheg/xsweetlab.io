# Developing components

Components are normal Node.js modules that follow some conventions to make them work well with PubSweet apps and tooling.

Components can contain either:

- [Express](https://expressjs.com/) routes and/or middleware to extend the server
- [React](https://facebook.github.io/react/) components and/or [Redux](http://redux.js.org/) functions to extend the client

By convention, when PubSweet components are published to [npm](http://npmjs.com) they use a name starting with `pubsweet-component-` to make them [easy to find](https://www.npmjs.com/search?q=pubsweet-component). However, this is not a requirement. In fact, components don't have to be published at all - they can live in a directory or a git repository.

Components use a specific format for the object exported from their [main file](https://docs.npmjs.com/files/package.json#main), allowing PubSweet to load them at the appropriate time and place. The exported object can have the keys `client` or `server` to specify where the component lives.

## Client components

For a client component, the value for the `client` key is an object which can have the following keys:

- `components` - an array of functions that return **React components**
- `actions` - a function that returns an array of **Redux actions**
- `reducers` - a function that returns an array of **Redux reducers**

So the structure of a client extension's main file will look something like this:

```js
module.exports = {
  client: {
    components: [
      () => someReactComponent,
      () => anotherReactComponent
    ],
    actions: () => arrayOfReduxActions,
    reducers: () => arrayOfReduxReducers
  }
}
```

Here's a real example from the [Login component](https://gitlab.coko.foundation/pubsweet/pubsweet-components/blob/master/packages/Login/index.js) that affects the `client`:

```js
module.exports = {
  client: {
    components: [
      () => require('./Login')
    ],
    actions: () => require('./actions'),
    reducers: () => require('./reducers')
  }
}
```

## Server components

For a server component, the value for the `server` key is a function that returns another function that accepts an [express](https://expressjs.com/) app as its first argument.

The function can do whatever it likes with the app - for example:

- adding new routes
- applying middleware functions

A server extension's main file looks like this:

```js
module.exports = {
  server: () => app => {
    app.get('someroute', someRouteFunction)
    app.use(someMiddleware)
  }
}
```

Here's a real example from the [Password Reset Backend component](https://gitlab.coko.foundation/pubsweet/pubsweet-components/blob/master/packages/PasswordResetBackend/index.js):

```js
module.exports = {
  server: () => app => require('./PasswordResetBackend')(app)
}
```
