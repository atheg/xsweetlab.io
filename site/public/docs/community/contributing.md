# Contributing

PubSweet is both an [Open Source software project](https://gitlab.coko.foundation/pubsweet/pubsweet) and an open community, part of the broader [Collaborative Knowledge Foundation](https://coko.foundation) community.

We welcome people of all kinds to join the community and contribute. Everyone is welcome in [our chat room](https://mattermost.coko.foundation/).

## Using the software

If you use PubSweet, please tell us about it.

You can do that by:

- adding your project to our project list
- [reporting software bugs](/docs/community/help.html) and/or contributing fixes
- [helping us improve this site](https://gitlab.com/pubsweet/pubsweet.gitlab.io/issues)
- [chatting to us](https://mattermost.coko.foundation/coko/channels/pubsweet)

## Contributing components

The component library is a resource that makes PubSweet better for everyone. The more components in the library, and the more people helping maintain them, the better the PubSweet ecosystem becomes for all of us.

- If you [develop components](/docs/components/developing.html), please add them to [the library](/docs/components/library.html).
- If you use components developed by others, please consider helping maintain them by reporting issues and contributing bug fixes.

## Contributing to the core modules

The [core modules](/docs/core/overview.html) are developed and maintained by a dedicated team and a community of contributors. Anyone is welcome to contribute ideas, issues, and code.

Development is managed in the [pubsweet project](https://gitlab.coko.foundation/pubsweet) on the [Coko Foundation GitLab instance](https://gitlab.coko.foundation).

We track high-level PubSweet issues and discussion in the [project-wide issue tracker](https://gitlab.coko.foundation/pubsweet/pubsweet/issues).

Each core module also has its own issue tracker:

- [`pubsweet-server`](https://gitlab.coko.foundation/pubsweet/pubsweet-server/issues)
- [`pubsweet-client`](https://gitlab.coko.foundation/pubsweet/pubsweet-client/issues)
- [`pubsweet-cli`](https://gitlab.coko.foundation/pubsweet/pubsweet-cli/issues)

### Guidelines for contributing to the core modules

#### Search for existing issues

If you've experienced a bug or want to discuss something in the issue trackers, please search first to see if an issue already exists.

#### Discuss contributions first

Please let us know about the contribution you plan to make before you submit it. Either comment on a relevant existing issue, or open new issue if you can't find an existing one. This helps us avoid duplicating effort and ensure contributions are likely to be accepted. You can also [ask us in the chat room](https://mattermost.coko.foundation/coko/channels/pubsweet) if you are unsure.

#### Use merge requests

We maintain `master` as the production branch and tag it with release names. If you want to contribute a bug fix or other code change, you should fork the appropriate repo, commit your changes in that repo, then create a merge request back to the `master` branch of the main repo.

#### Merge request workflow

Merge requests should be marked as `WIP: ` until they are completely ready to be reviewed.

When you think the MR is ready, remove the `WIP` label and a member of the PubSweet team will review the changes. Once any requested changes or problems have been resolved, the MR will be considered for merging.

Before merging all MRs must fulfill these three simple rules:

1. It must pass the tests.
2. It must not reduce the test coverage.
3. If the MR is a bugfix, it must include a regression test.
