# Installing Prerequisites

* [Mac (OSX)](#mac)
* [Linux (Ubuntu v16.04)](#linux)
* [Windows 10 (not fully supported)](#windows)

<h2><span id="mac">Mac (OSX):</span></h2>

### 1. Install and configure `git`

Use this installer to install git: https://git-scm.com/download/mac

Once `git` is installed, configure it with the following commands, with your own name and email:
```bash
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

### 2. Install `nvm`

We recommend using [`nvm`](https://github.com/creationix/nvm) to install and manage Node.js and nvm versions. To get nvm, run:
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```

### 3. Install `Node.js`

To install or switch to node v7.7 with nvm, run:
```bash
nvm install 7.7
```

### 4. Use `npm` v5.3

To install or switch to npm v5.3, run:
```bash
npm install -g npm@5.3
```

### 5. Install `yarn`

Run the following command:
```bash
curl -o- -L https://yarnpkg.com/install.sh | bash
```

### 6. Double check versions

To double check that you are using the correct node and npm versions, run  
`nvm current`: should be 7.7.4  
`npm --version`: should be 5.3.0  
If these are correct, you should be ready to [install PubSweet](/docs/core/cli.html)

<h2><span id="linux">Linux (Ubuntu v16.04):</span></h2>

### 1. Prepare for tool installation
```bash
sudo apt-get update
sudo apt-get install build-essential libssl-dev
```

### 2. Install and configure `git`

```bash
sudo apt-get install git-all
```
Once `git` is installed, configure it with the following commands, with your own name and email:
```bash
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

### 3. Install `nvm`
Download the nvm installation script:
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```
Restart the terminal to complete installation.

### 4. Install `Node.js`

To install or switch to node v7.7 with nvm, run:
```bash
nvm install 7.7
```

### 5. Use `npm` v5.3

To install or switch to npm v5.3, run:
```bash
npm install -g npm@5.3
```

### 6. Install `yarn`

Run the following command:
```bash
curl -o- -L https://yarnpkg.com/install.sh | bash
```
Restart the terminal to complete installation.

### 7. Double check versions

To double check that you are using the correct node and npm versions, run  
`nvm current`: should be 7.7.4  
`npm --version`: should be 5.3.0  
If these are correct, you should be ready to [install PubSweet](/docs/core/cli.html)

<h2><span id="windows">Windows 10:</span></h2>

Note that PubSweet is not fully supported on PC. If you are using Windows 10, we recommend you use the Bash terminal for Windows. See below for additional instructions on running PubSweet on Windows

### 1. Enable the Linux Bash shell

The Linux Bash shell requires a 64-bit version of Windows 10 Anniversary Update (build 1607+). Follow these instructions to [enable the Linux Bash shell on Windows 10](https://msdn.microsoft.com/en-us/commandline/wsl/install_guide).

### 2. Install `nvm`

We recommend using [`nvm`](https://github.com/creationix/nvm) to install and manage Node.js and nvm versions. To get nvm, run:
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```
Once this command has completed, it tells you to restart your terminal or run two commands to begin using nvm. You may have to manually run the commands:
```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
```
Once the Bash terminal recognizes the nvm command, you'll know it's installed.

### 3. Install `Node.js`

To install or switch to node v7.7 with nvm, run:
```bash
nvm install 7.7
```

### 4. Use `npm` v5.3

To install or switch to npm v5.3, run:
```bash
npm install -g npm@5.3
```

### 5. Install `yarn`

Run the following command:
```bash
curl -o- -L https://yarnpkg.com/install.sh | bash
```

### 6. Double check versions

To double check that you are using the correct node and npm versions, run  
`nvm current`: should be 7.7.4  
`npm --version`: should be 5.3.0  
If these are correct, you should be ready to [install PubSweet](/docs/core/cli.html)

<h3><span id="pc-note">Running on Windows:</span></h3>

To create and run a new PubSweet app with Bash on Windows 10, follow these instructions:
1. Install PubSweet with `npm install -g pubsweet`. Ignore the leveldown failure error.
2. Generate an app with `pubsweet new [appname]`. This will create the app's directory structure but fail to setup the database.
3. `cd` into your new app directory: run `cd [appname]` and `npm install` to install dependencies
5. Now, the `pubsweet setupdb ./` will work. Run it and complete the prompts
6. You should now be able to start the PubSweet server with `pubsweet run` and access your app on `localhost:3000`
