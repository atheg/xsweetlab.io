# the open toolkit for building publishing workflows

**PubSweet** is a free, open source toolkit for building state-of-the-art publishing workflows.

It's designed to be modular and flexible, consisting of a [**server**](/docs/core/server.html) and [**client**](/docs/core/client.html) that work together, [**components**](/docs/components/index.html) that can modify or extend the functionality of the server and/or client, and a [**command-line tool**](/docs/core/cli.html) that helps manage PubSweet apps.

PubSweet is [being used](/docs/community/projects.html) for book publishing, and academic journal production and preprint management services are in development. By drawing on the [growing library of components](/docs/components/library.html), PubSweet can be used to rapidly create bespoke publishing systems. If the existing components don't completely meet your needs, you can focus development on [building new components](/docs/components/developing.html) to provide just the new functionality needed. Join the [PubSweet community](/docs/community/contributing.html) and help us build a common resource of open components for publishing by [contributing components back](/docs/components/library.html).

You can read more about the ideas behind PubSweet at the [Coko Foundation site](https://coko.foundation/technology/). If you would like to talk to anyone for help working with PubSweet you can find us all on the [Coko Chat](https://mattermost.coko.foundation).

---

<p class="center">**Get started** by learning about the [pubsweet core modules](/docs/core/overview.html).</p>
